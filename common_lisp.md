### =
Returns true when its arguments are numerically equal -- when the difference between them is zero.
```lisp
> (= 1 1.0)
T
> (eql 1 1.0)
NIL
```
It is less strict than `eql`, which also requires its arguments to be of the same type.

### *query-io*
A global variable that contains the input stream connected to the terminal.

### adjoin
It takes an object and a **list**, and conses the object onto the list only if it is not already a member.
```lisp
> (adjoin 'b '(a b c))
(A B C)
> (adjoin 'z '(a b c))
(Z A B C)
```
It also takes the same keyword arguments as `member`.

### and
Take any number of arguments, but only evaluate as many as they need to in order to decide what to return. If all its arguments are true, then and returns the value of the last one.
```lisp
> (and t (+ 1 2))
3
```

### append
Returns the concatenation of any number of **list**.
```lisp
> (append '(a b) '(c d) '(e))
(A B C D E)
```

### apply
Takes a *function* and a *list of arguments* for it, and returns the result of applying the function to the arguments.
```lisp
> (apply #'+ '(1 2 3))
6
```
This is useful when you have a function with a fixed number of arguments and you have a list of its argument that you want to pass them to the function.
```lisp
> (defun take-two (one two) (list one two))
> (defvar one-and-two '(1 2))
> (apply #'take-two one-and-two)
(1 2)
```
`apply` can also accept "loose" arguments as long as the last argument is a list.
`apply` doesn't care about whether the function being applied takes `&optional`, `&rest`, or `&key` arguments -- the argument list produced by combining any loose arguments with the final list must be a legal argument list for the function with enough arguments for all the required parameters and only appropriate keyword parameters.

### aref
Retrieves an array element.
```lisp
> (setf arr (make-array '(2 3) :initial-element nil))
> (aref arr 0 0)
NIL
```
We can replace some element of an array by using `setf`.
```lisp
> (setf (aref arr 0 0) 'b)
B
> (aref arr 0 0)
B
```

### array-dimension
Return the axis-number dimension of array, that is the length of the *i*th dimension.
```lisp
> (array-dimension (make-array 4) 0)
4
> (array-dimension (make-array '(2 3) 1))
3
```
Note that `(array-dimension array n)` is equivalence to `(nth n (array-dimension array)`.

### array-dimensions
Return a list containing the legth of the array's dimension.

### array-rank
Returns the number of the dimensions of array.
```lisp
> (array-rank (make-array '(4)))
1
> (array-rank (make-array '(2 3)))
2
```

### array-total-size
Returns the array total size of the array, that is how many elements will fit in the array.
```lisp
> (array-total-size (make-array 4))
4
> (array-total-size (make-array 0))
0
> (array-total-size (make-array '(4 2)))
8
> (array-total-size (make-array '(4 0)))
0
> (array-total-size (make-array '()))
1
```
Note that `(array-total-size x)` is equivalence to `(reduce #'* (array-dimension x))`.

### assert
It takes a test expression and a **list** of one or more places, follewed by the arguments you migth give to `error`:
```lisp
> (let ((sandwich '(ham on rye)))
    (assert (eql (car sandwich) 'chicken)
            ((car sandwich))
            "I wanted a ~A sandwich." 'chicken)
            sandwich)
Error: I wanted a CHICKEN sandwich.
Options: :abort, :backtrace, :continue
>> :continue
New value of (CAR SANDWICH)?
'chicken
(CHICKEN OF RYE)
>
```

### assoc
Retrieve the pair associated with a given key. If it doesn't find what it's looking for, it returns `nil`.
```lisp
> (setf trans '((+ . "add") (- . "subtract")))
((+ . "add") (- . "subtract"))
> (assoc '+ trans)
(+ . "add")
> (assoc '* trans)
NIL
```
Like `member`, the `assoc` takes key word arguments, including `:test` and `:key`. Common Lisp also defines an `assoc-if`, which is to `assoc` what `member-if` is to `member`.

### atom
Return true if the given parameter is an atom.
```lisp
> (atom 1)
T
```

### block
Like `progn` with a name and an emergency exit. The first argument should be a _symbol_. This becomes the name of the block. At any point within the body, you can halt evaluation and return a value immediately by using `return-from` with the block's name.
```lisp
> (block head
    (format t "Here we go.") 
    (return-from head 'idea)
    (format t "We'll never see this."))
Here we go.
IDEA
```
The second argument to `return-from` is returned as the value of the block named by the first. Expressions after the `return-from` are not evaluated.
The body of a function defined with `defun` is implicitly enclosed in a block with the same name as the function, so we can say:
```lisp
> (defun foo ()  (return-from foo 27))
```

### boundp
Check whether the symbol is the name of a global variable or constant.
```lisp
> (defparameter *glob* 1)
> (boundp '*glob*)
*GLOB*
T
```

### butlast
Returns the list without the last element.
```lisp
> (butlast '(1 2 3))
(1 2)
```
Its destructive version is `nbutlast`.

### call-next-method
See `defmethod`. Calls the next primary method (if any) after the aronud method.

### car
Take the first element of the list.
```lisp
> (car '(a b c))
A
```

### case
Compares a value against a series of constants:
```lisp
(defun month-length (mon)
    (case mon
        ((jan mar may jul aug oct dec) 31)
        ((apr jun sept nov) 30)
        (feb (if (leap-year) 29 28))
        (otherwise "unknown month")))
```
A `case` expression begins with an argument whose value will be compared against the keys in each clause. Then comes zero or more clauses, each one beginnig with either a key, or a list of key, followed by zero or more expressions. The keys are threated as constants; they will not be evaluated. The value of the first argument is compared (using `eql`) to the key/s at the head of each clause. If there is a match, the expressions in the rest of that clause are evaluated, and the value of the last is returned as the value of the `case`.
The default clause may have the key `t` or `otherwise`. If no clause succeeds, or the successful clause contains only key, then the case returns `nil`.
```lisp
> (case 99 (99))
NIL
```

### catch
Takes a tag, which can be any kind of object, follewed by a body of expressions. Also see `throw`.
```lisp
> (defun super()
    (catch 'abort
        (sub)
        (format t "We'll never see this.")))
> (defun sub ()
    (throw 'abort 99))
```
The expressions are evaluated in order, as if in a `progn`. At any point within this code or code called by it, a `throw` with the corresponding tag will cause the catch expression to return immediately.
```lisp
> (super)
99
```

### cdr
Take the rest elements after the first element.
```lisp
> (cdr '(a b c))
(B C)
```

### ceiling
Returns the least integer greater than or equal to its argument. It also return as a second value the difference between the argument and the first value.

### char<
This function, along with `char<=`, `char=`, `char>=`, `char>`, and `char/=` compare characters. They work like the numeric comparison opeartors we described.
```lisp
> (sort "elbow" #'char<)
"below"
```

### char
Because strings are vectors, both sequence functions and array functions work on them. Ex.
```lisp
> (aref "abc" 1)
#\b
```
But using `char` is faster.
```lisp
> (char "abc" 1)
#\b
```

### char-code
Returns the number associated with a character.

### check-type
It takes a *place*, a *type name*, and an optional *string*, and signals a *correctable error* if the value of the place is not of the designated type. The handler for a correctable error will give us the option of providing a new value:
```lisp
> (let ((x '(a b c)))
    (check-type (car x) integer "an integer")
    x)
Error: The value of (CAR X), A, should be an integer.
Options: :abort, :backtrace, :continue
>> :continue
New value of (CAR X)?
> 99
(99 B C)
```

### clrhash
Removes all entries from the hash table and then returns that empty hash table.

### coerce
We can use this to convert a string of length 1 to a character. We can also use this to convert any sequence of characters into a string. We can't use this to convert a character to a string, though - we'll have to use `string` instead.
```lisp
> (coerce "a" 'character)
#\a
> (coerce (subseq "cool" 2 3) 'character)
#\o
> (coerce "cool" 'list)
(#\c #\o #\o #\l)
> (coerce '(#\h #\e #\y) 'string)
"hey"
> (coerce (nth 2 '(#\h #\e #\y)) 'character)
#\y
> (defparameter *my-array* (make-array 5 :initial-element #\x))
*MY-ARRAY*
> *my-array*
#(#\x #\x #\x #\x #\x)
> (coerce *my-array* 'string)
"xxxxx"
> (string 'howdy)
"HOWDY"
> (string #\y)
"y"
> (coerce #\y 'string)
#\y can't be converted to type STRING.
[Condition of type SIMPLE-TYPE-ERROR]
```

### code-char
Returns the character associated with a number.

### compile-file
Compile the given file into fsl file. The name given to `compile-file` is the lisp file without extension, i.e. like when we use `load`.

### complement
It takes a predicate and returns the opposite predicate.
```lisp
> (mapcar (complement #'oddp) '(1 2 3 4 5 6))
(NIL T NIL T NIL T)
```

### cond
It allows multiple conditions, and the code associated with each has an implicit `progn`. It's intended for use in situations where we would otherwise have to make the third argument of an `if` another `if`.
```lisp
(defun our-member (obj lst)
    (if (atom lst)
        nil
        (if (eql (car lst) obj)
            lst
            (our-member obj (cdr lst)))))
```
could also be defined as
```lisp
(defun our-member (obj lst)
    (cond
        ((atom lst) nil)
        ((eql (car lst) obj) lst)
        (t 
            (our-member obj (cdr lst)))))
```
In general, `cond` takes zero or more arguments. Each one must be a list consisting of a condition followed by zero or more expressions. When the `cond` expression is evaluated, the conditions are evaluated in order until one of them returns true. When it does, the expressions associated with it are evaluated in order, and the value of the last is returned as the value of the `cond` If there are no expressions after the successful condition, the value of the condition itself is returned as example below.
```lisp
> (cond (99))
99
```
A `cond` clause with a condition of `t` will always succeed, it is conventional to make it a default clause have `t` as the condition. If no clause succeeds, the `cond` return `nil`, but it is a bad style.

### cons
build a list. If its second argument is a list, it returns a new list with the first argument added to the front:
```lisp
> (cons 'a '(b c d)
(A B C D)
```

### concatenate
It takes a symbol indicating the type of the rusult, plus one or more sequences.
```lisp
> (concatenate 'string "not " "to worry")
"not to worry"
> (concatenate 'list "Karl" " " "Mark")
(#\K #\a #\r #\l #\Space #\M #\a #\r #\x)
```

### copy-list
Takes a list and return a copy of it. The new list will have the same elements, but contained in new conses.
```lisp
> (setf x '(a b c)y (copy-list x))
(A B C)
```

### copy-seq
Copy a sequence.

### count
It's of the form `(count foo sequence)`.
Return the number of elements in sequence that match *foo*. Other parmeters are `:from-end`, `:start`, and `:end`.
There are also `count-if` and `count-not` which have the form `count-if *(test-function sequence)`, and it's the same form for `count-not`.

### decf
Decreses it argument. An expression of the form `(decf x n)` is similar in effect to `(setf x (- x n))`. The second argument is optional and defaults to 1.

### defclass
The first argument is class name, the second argument is a list of superclasses.
```lisp
(defclass colored ()
    (color))
```
This define a colored class with color slot.
The third argument is a list of slot definitions. In its simplest form, they are symbols representing their names. We can give them one or more properties. Properties are specified like keyword arguments.
`:accessor`, it implicitly define a function that refers to the slot, making it unnecessary to call `slot-value`.
```lisp
(defclass circle ()
    ((radius :accessor circle-radius)
     (center :accessor circle-center)))
```
Now we can refer to the slots as `circle-radius` and `circle-center`, respectively. With `:accessor` we can read the slot, or we can set its value via `setf`. `:reader` makes we can only read the slot and `:writer` makes we can only set the slot's value.
`:initform` specifies a default value for a slot while `:initarg` makes we can be able to initialize the slot in the call to `make-instance`.
```lisp
(defclass circle ()
    ((radius :accessor circle-radius
             :initarg :radius
             :initform 1)
     (center :accessor circle-center
             :initarg :center
             :initform (cons 0 0)))))
```
When we make an instance of a `circle` we can either pass a value for a slot using the keyword parameter defined as the slot's `:initarg`, or let the value default to that of the slot's `:initform`.
```lisp
> (setf (make-instance 'circle :radius 3))> (circle-radius c)3
> (circle-center c)(0 . 0)> (setf (circle-center c) '(5 5))> (circle-center c)(5 5)
```
Note that `:initarg`s take precedence over `initform`s.
To make the same slots have the same value for *every* instance, we declare the slot with `:allocation :class`. (The default, which makes the same slot doesn't have the same value for every instance, is `:allocation :instance`.) When we change the value of such a slot in one instance, that slot will get the same value in every other instance.
`:documentation`, if given, should be a string to serve as the slot`s documentation.
`:type`, specifies that the slot will only contain elements of that type.

### declaim
Make global declarations. It should be followed by one or more declaration forms.
Those declaration forms including:
- `optimize` which can be specify the optimization level. Its parameters can be `speed` which is the speed of the code produced by the compiler, `compilation-speed` refers to the speed at which the program will be compiled, `safety` refers to the amount of error-checking done in the object code, `space` refers to the size and memory needs of the object code, and `debug` refers to the amount of information retained for debugging. These parameters can be assigned weigths from 0 (unimportant) to 3 (most important) indeclarations.
- `inline`, followed by the function symbol. This make the reference to the function within a compiled function should no longer require a real function call, for example:
```lisp
(declaim (inline single?))(defun single? (lst)(and (consp lst) (null (cdr lst))))(defun foo (x)(single? (bar x)));; and when foo is compiled, the code for single? should be compiled rigth into it, just as if we had written
(defun foo (x)(let ((lst (bar x)))(and (consp lst) (null (cdr lst))))
```
Its limitations are recursive functions can't be inlined and if an inlined function is redefined, we have to recompile any function that calls it, or the calling function will still reflect the old definition.
- `type`, it is a lits containing the symbol `type`, followed by a type name and the names of one or more variables.
`(declaim (type fixnum *count*))`
In ANSI Common Lisp, you can omit the `type` and just say simply:
`(declaim (fixnum *count*))`

### declare
See `declaim`. It's like `declaim` but for local use. Declarations can begin any body of code where variables have just been created; in `defun`, `lambda`, `let`, `do` and so on. To declar a function's parameters to be `fixnum`s, for example, we would say:
```lisp
(defun poly (a b x)(declare (fixnum a b x))(+ (* a (expt x 2)) (* b x)))
```
A variable name in a type declaration refers to the variable with that name in the context where the declaration occurs -- to the variable whose value would be altered if it were instead an assignment.
The most general form of array declaration consists of the array type followed by the element type and a list of dimensions.
```lisp
(declare (type (vector fixnum 20) v))
```
The code above declares v to ba a vector of length 20, specialized for fixnums.
```lisp
(declare (type (simple-array fixnum (4 4)) ar))
```
The code above declares ar to be a 4x4 simple array specialized for fixnums.

### defconstant
This creates a global parameter but it will give an error if local parameter will the same name are created.
```lisp
(defconstant limit (+ *SOME-GLOBAL* 1))
```

### define-modify-macro
It's like a restricted class of macro on `setf`. It takes three argumentns: the name of the macro, its additional parameters (the place is implicitly the first), and the name of a function that yields the new value of the place.

### defun
Define the function. It takes three or more arguments: a name, a list of parameters, and one or more expression that will make up the body of the function
```lisp
> (defun name (list-of-arguments)(body-of-function))NAME
```
By making the first argument to `defun` a list of the form `(setf f)`, we difine what happens when the first argument to `setf` is a call to `f`. In the definition of a function of this form, the first parameter represents the new value, and the remaining parameters represent arguments to f. That is, it is of the form:
```lisp
(defun (setf <name>) (new-value <other arguments>)body)
```
The following pair of functions define `primo` as a synonym for `car`.
```lisp
> (defun primo (lst) (car lst))> (defun (setf primo) (val lst) (setf (car lst) val))
```
When we call `setf` with `primo`, it will call to the latter function above.
```lisp
> (let ((x (list 'a 'b 'c)))(setf (primo x) 480)x)(480 B C)
```
It's not necessary to define `primo` in order to define `(setf primo)`, but such definitions usually come in pairs.
Another example on `(defun (setf f))`:
```lisp
> (defparameter *current-name* "")> (defun hello (name)(format t "hello ~A~&" name))> (defun (setf hello) (nev-value)(hello new-value)(setf *current-name* new-value)(format t "current name is now ~a~&" new-value))> (setf (hello) "Alice")hell Alice
current name is now Alice
NIL
```
The string, if exists, as the first experssion in the body of a function is documentation string which can be retrieved by calling `documentation`.
The token `&rest`, which can be inserted before the last variable in the parameter list of a function, when the function is called, this variable will be set to a list of all the remaining arguments.
The token `&optional` is used when the arguments could be omitted, and would default to certain values. If the symbol `&optional` is in the parameter list of a function, then all the arguments after it are optional, and default to `nil`. The default for an optional parameter can be any Lisp expression. If this expression isn't a constant, it will be evaluated anew each time a default is needed. Moreover, the default-value expression can refer to parameters that occur ealier in the parameter list. You can also check whether the caller supplied the value or not by adding another parameter specifier after the default-value expression. This value are usuaslly named the same as the actual parameter with a `-supplied-p` on the end.
```lisp
(defun foo (a &optional (b 1)) (list a b)) ; b is default to 1
(defun bar (a &optional (b a)) (list a b)) ; b is defualt to be the same value as a
(defun func (a &optional (b 1 b-supplied-p)) (list a b b-supplied-p)) ; if b is supplied by the caller, the argument b-supplied-p will be t
```
The token `&key` is like the optional parameter, all the parameters after the symbol `&key` are optional. And when the function is called, these parameters will be identified not by their position, but by symbolic tags that precede them. &key must comes after required, &optional and &rest parameters Of course we can have a default value for `&key` just like we can with `&optional`. This token also support *supplied-p* parameter, which is will be set to true or false depending on whether an argument was actually passed for that keyword parameter. `&key` also can refer to parameters that aappear earlier in the parameter list, just like `&optional`.
```lisp
> (defun foo (&key a (b 20) (c 30 c-p)) (list a b c c-p))> (foo :a 1 :c 3)(1 20 3 T)> (foo)(NIL 20 30 NIL)
```
If you want the keyword that the caller uses to specify the parameter to be different from the name of the actual parameter, you can replace the parameter name with another list containing the keyword to use when calling the function and the name to be used for the parameter. For example:
```lisp
> (defun foo (&key ((:apple a)) ((:box b) 0) ((:charlie c) 0 c-supplied-p)) (list a b c c-supplied-p))FOO
> (foo :apple 10 :box 20 :charlie 30)(10 20 30 T)
```
It's usedful when you want to use short variable names internally but descriptive keywords in the API.
Keywords and their associated arguments can be collected in *rest parameters* and passed on to other functions that are expecting them. That is, any arguments remaining after values have been doled out to all the required and optional parameters are gathered up into a list that becomes the value of the &rest parameter.
```lisp
> (defun our-adjoin (obj lst &rest args)(if (apply #'member obj lst args)lst
(cons obj lst)))
```
Since `adjoin` takes the same keyword arguments as `member`, we just collect them in a rest argument and pass them onto `member`.
The token `&allow-other-keys` enables us to pass non-related key to to the caller without casing any error. We might need it when passing around arguments or with higher level manipulation of functions. 
Note that the *supplied parameter* can be used as subfix `-p` and `-supplied-p`.

### defgeneric
It has no body, instead, it takes the parameters that must be accepted by all the methods that will be defined on the generic function.
`:method-combination` makes the generic method combines all the results of appopriated methods. And the second paramether of `defmethod` must be that symbol of that combination method, i.e. we can't use `:before`, `:around`, and `:after` anymore.
```lisp
(defgeneric price (x):method-combination +))(defclass jacket () ())(defclass trousers () ())(defclass suit (jacket trousers) ())
(defmethod price + ((ju jacket)) 350)(defmethod price + ((tr trousers)) 200)
> (price (make-instance 'suit))550
```
The following symbols can be used as the second argument to `defmethod` or in the `:method-combination` option to `defgeneric`:
`+ and append list max min nconc or progn`. You can also use `standard`, which yields standard method combination.

### defmacro
It's like a `defun` but it defines how a call should be translated.
In a backquoted expression, if we prefix a comma to something, it will be evaluated.
```lisp
> (setf a 1 a 2)2
> `(a is ,a and b is ,b)(A IS 1 AND B IS 2)
```
Using comma-at will splice its arguments (wich should be a list).
```lisp
> (setf lst '(a b c))(A B C)> `(lst is ,lst)(LST IS (A B C))> `(its elements are ,@lst)(ITS ELEMENTS ARE A B C)
```

### defmethod
It's like a `defun`,but we can continue to add new method to it and, when it is called, it will use the most specific method defined by `defmethod`.
```lisp
> (defmethod combine (x y)(list x y))> (defclass circle () radius center)> (setf c (make-instance 'circle))> (defmethod combine ((cc circle) y)(list (slot-value 'radius cc) y))
```
In the code above, if we pass an instance of `circle` to combine, it will use the second `combine` defined by `defmethod` because it's more specific. An unspecialized method is like a safety net, like an `otherwise` clause in a `case` expression.
Methods can be specialized on types, not just the ones that were defined by `defclass`.
```lisp
(defmethod combine ((x number) (y number))(+ x y))
```
Here, this method will be called when both arguments are numbers.
```lisp
(defmethod combine ((x eql ('powder)) (y (eql 'spark)))'boom)
```
This method specialized on individual objects, as determined by `eql`.
Method's parameters must have the same number of required parameters, the same number of optional parameters (if any), and must either all use `&rest` or `&key` ,or all not use them.
If we define another method with the same qualifiers and specializations, it overwrites the original one.
We can also have auxiliary methods byhputting a qualifying keyword, `:before`, `:after`, and `:around`, after the method name. When the `:around` is used, the value returned by the around-method is returned as the value of the generic function.
```lisp
(defclass speaker () ())(defmethod speak ((s speaker) string)(format t "~A" string))> (speak (make-instance 'speaker) "I'm hungry")I'm hungry
NIL
(defclass intellectual (speaker) ())(defmethod speak :before ((i intellectual) string)(princ "Perhaps "))(defmethod speak :after ((i intellectual) string)(princ " in some sence"))> (speak (make-instance 'intellectual) "I'm hungry")Perhaps I'm hunngry in some sense
NIL
(defmethod speak :before ((s speaker) string)(princ "I think "))> (speak (make-instance 'intellectual) "I'm hungry")Perhaps I thing I'm hungry in some sense
NIL
(defclass countier (speaker) ())(defmethod speak :around ((c countier) string)(format t "Does the King believe that ~A? " string)(if (eql (read) 'yes)(if (next-methods-p) (call-next-method))(format t "Indeed, it is a preposterous idea.~%"))'bow)> (speak (make-instance 'courtier) "kings will last")Does the King believe that kings will last? yes
I think kings will last
BOW
> (speak (make-instance 'courtier) "the world is round")Does the King believe that the world is round? no
Indeed, it is a preposterous idea.
BOW
```

### defparameter
defparameter creat a global variable by giving a symbol and a value. _If we create the new local variable with the same name, then the local one will use instead._ `defparameter` always assign a new value to the variable.
```lisp
> (defparameter *glob* 99)*GLOB* ; this parameter will be visible for all function call
> *glob*
99
> (defparameter *glob* 1)*glob*
> *glob*
1
```

### defpackage
Define a new package.
```lisp
> (defpackage "MY-APPLICATION")(:use "COMMON-LISP")(:nicknames "APP")(:export "A"))
```
The keyword `:use` means we use the argument package, which will be accessible without package qualifiers.
`:nicknames` means other package can be refer to this package's symbols as, e.g. `app:a`.
`:export` means other packages can only access those symbols we export.

### defstruct
In the simplest case we just give the name of the structure and the names of the fields:
```lisp
(defstruct point
x
y)
```
It also implicitly defines the functions `make-point`, `point-p`, `copy-point`, `point-x`, and `point-y`.
Each call to `make-point` will return a new point. We can specify the values of individual fields by giving the corresponding keyword arguments:
```lisp
> (setf p (make-point :x 0 :y 0))#S(POINT X 0 Y 0)
```
The access functions for `point` fields are defined not only to retrieve values, but to work with `setf`.
```lisp
> (point-x p)0
> (setf (point-y p) 2)2
> p
#S(POINT X 0 Y 2)
```
Defining a structure also defines a type of that name. Each point will be of type `point`, then `structure`, then `atom`, then `t`. So as well as using `point-p` to test whether something is a point,
```lisp
> (point-p p)T
> (typep p 'point)T
```
We can also use general purpose functions like `typep`.
We can specify default values for structure fields by enclosing the field name and a default expression in a list in the original definition.
```lisp
> (defstruct point2 (x 1) (y 1))> (make-point2)#S(POINT2 X 1 Y 1)
```
We can also control things like the way a structure is displayed, and the prefix used in the names of the access functions it creates.
```lisp
> (defstruct (point (:conc-name p) (:print-function print-point))(x 0)(y 0))> (defun print-point (p stream depth)(format stream "#<~A,~A>" (px p) (py p)))
```
The `:conc-name` argument specifies what should be concatenated to the front of the field names to make access functions for them. By default it was `point-`; now it will be simply `p`.
The `:print-function` is the name of the function that should be used to print a point when it has to be displayed -- e.g. by the toplevel. This function takes three arguments: the structure to be printed, the place where it is to be printed, and a third argument that can usually be ignored. In ANSI Common Lisp, we can give instead a `:print-object` argument, which only takes the first two arguments.
The function `print-point` will display points in an abbreviated form:
```lisp
>(make-point)#<0,0>
```
We can specify the type of the we specify the default value.
```lisp
(defstruct person
id
(name "A" :type string)age)
```
We can set the structure's constructor so as to create the structure without using keyword arguments, which can be more convenient somethimes. We give it a name and the order of the arguments.
```lisp
(defstruct (preson (:constructor create-person (id name age)))id
name
age)
```
Now we can do this:
```lisp
(create-person 1 "me" 7)
```
However, the defaul `make-person` doesn't work anymore.
We canuse single inheritance with the `:include <struct>` argument:
```lisp
(defstruct (female (:include person))(gender "female" :type string))(make-female :name "Little");; #S(FEMALE :ID NIL :NAME "lITTLE" :AGE NIL :GENDER "female")
```

### defvar
Assign a value to a parameter but `defvar` does it only once.
```lisp
> (defvar x 1)x
> (dafvar x 2)x
> x
1
```

### destructuring-bind
It is a generalization of `let`. Instead of single variables, it takes a *pattern* -- one or more variables arranged in the form of a tree -- and binds them to the corresponding parts of some actual tree.
```lisp
> (destructuring-bind (w (x y) . z) '(a (b c) d e)(list w x y z))(A B C (D E))
```
Each subtree in the pattern given as the first argument may be as complex as the parameter list of a function, it can use `&optional`, `&rest`, and `&key`.
```lisp
> (destructurnig-bind ((&key w x) &rest y) '((:w 3) a) (list w x y))(3 NIL (A))
```
The `&whole` parameter is bound to the whole list. It must be the first one and other can follew.
```lisp
> (destructuring-bind (&whole whole-list &key x y z) (list :z 1 :y 2 :x 3)(list :x x :y y :z z :whole whole-list))(:X 3 :Y 2 :Z 1 :WHOLE-LIST (:Z 1 :Y 2 :X 3))
```
We can also give default values.
```lisp
> (destructuring-bind (&key a (b :not-found) c &allow-other-keys)'(:c 23 :d "D" :a #\A :foo :whatever)(list a b c))(#\A :NOT-FOUND 23)
```

### digit-char-p
Tests whether a character is a digit, and also returns the corresponding integer.

### documentation
Returns the function's documentation strign.

### do
The first argument is a list of variable specifications. Each element of this list can be of the form `(variable initial update)` when *variable* is a symbol, and *initial* and *upadate* are expreesions.
The second argument to `do` should be a list containing one or more expression. The first expression is used to test whether iteration should stop. The remaining expressions in this list will be evaluated in order when iteration stops, and the value of the last will be returned as the value of the do.
The remaining arguments to `do` comprise the body of the loop. They will be evaluated, in order, on each iteration.
```lisp
> (defun show-sequares (start end)(do ((i start (+ i 1)))((> i end) 'done)(format t "~A ~A~%" i (* i i))))> (show-squares 1 2)1 1
2 4
DONE
```
The *initial* and *update* forms are optional. If the *update* form is omitted, the variable won't be updated on successive iterations. If the *inital* form is also omitted, the variable will be initially `nil`.
If an *update* form refers to a variable that has its own *update* form, it gets the value from the previous iteration.
```lisp
> (let ((x 'a))(do ((x 1 (+ x 1))(y x x))((> x 5))(format t "(~A ~A) " x y)))(1 A) (2 1) (3 2) (4 3) (5 4)NIL
```
If you want to refer to a variable form a previous clause, use `do*`.
`do` contains both an implicit `block` and an implicit `tagbody`, it's possible to use `return`, `return-from`, and `go` within the body of a `do`.

### do*
Like what `let*` is related to `let`. Any *initial* or *update* form can refer to a variable from a previous clause, and it will get the current value.
```lisp
> (do* ((x 1 (+ x 1))(y x x))((> x 5))(format t "(~A ~A) " x y))(1 1) (2 2) (3 3) (4 4) (5 5)NIL
```
### dolist
It iterates through the elements of a list. It takes an argument of the form (variable expression), followed by a body of expressions. The body will be evaluated with *variable* bound to successive elements of the list returned by *expression*.
```lisp
> (defun our-length (lst)(let ((len 0))(dolist (obj lst)(setf len (+ len 1)))len)
```
The third expression within the initial list will be evaluated and returned as the value of `dolist` when iteration terminates. It defaults to `nil`.
```lisp
> (dolist (x '(a b c d) 'done)(format t "~A " x))A B C D
DONE
```
We can also use the third expression within the initial list will be evaluated and returned as the value of `dolist` when iteration terminates.
```lisp
(defun length (list)(let ((len 0))(dolist (element list len)(incf len))))
```
### dotimes
The first argument is assigned to 0, 1, 2 etc., to second argument minus one. The second argument is how many times the macro loops. The last argument is what will be returned.
```lisp
> (let (value)(dotimes (number 3 value)(setq value (cons number value))))(2 1 0)
```
The third expression in the initial list is optional and defaults to `nil`. Also it can refer to the interation variable.
```lisp
> (dotimes (x 5 x)(format t "~A " x))0 1 2 3 4
5
```

### dynamic-extent
Use with `declare`. It's used to avoid garbage collection by encoraging the compiler to allocate objects on the stack instead of the heap (the compiler may not do that though). By giving a dynamic extent declaration for a variableyou're saying that the variable's value need not last any longer than the variable does.

### ecase
It is like `case` but signals an error if none of the keys match:
```lisp
> (ecase 1 (2 3) (4 5))Error: No applicable clause.
Options: :abort, :backtrace
>>
```
The regular `case` will return `nil` if no key matches, but since it's bad style to take advantage of this return value, you migth as well use `ecase` whenever you don't have an otherwise clause.

### elt
Retrieves elements of sequences.
```lisp
> (elt '(a b c) 1)B
```
However, using `aref` for arrays, `svref` for vectors, `nth` for list, and `char` for strings is faster. `elt` should be used in code that is supposed to work for sequences generally.

### eql
Test whether its two arguments are identical (i.e., the same object).
```lips
> (eql 1 1)t
```

### equal
Return true if its argements would print the same.
```lisp
> (setf x (cons 'a nil))> (equal x (cons 'a nil))T
```

### error
Calling `error` interrupts execution, but instead of transfers control to another point higher up in the calling tree, it transfers control to the Lisp error handler.
```lisp
> (progn
(error "Oops!")(format t "After the error."))Error: Oops!
Options: :abort, :backtrace
>>
```
Another way to call it is to give it the same arguments that you migth pass to `format`.
```lisp
> (error "Your report uses ~A as a verb." 'status)Error: Your report uses STATUS as a verb.
Options: :abort, :backtrace
>>
```

### eval
Takes an expression, evaluates it, and returns its value.
```lisp
> (eval '(+ 1 2 3))6
> (eval '(format t "Hello"))Hello
NIL
```

### every
The same as `some`. Takes a predicate and one or more sequences. When given just one sequence, it test whether the elements satisfy the predicate. If they are given more than one sequence, the predicate must take as many arguments as there are sequennces, and arguments are drawn one at a time from all the sequences. If the sequences are of different lengths, the shortest one determines the number of test performed.
```lisp
> (every #'oddp '(1 3 4))T
> (every #'> '(1 3 5) '(0 2 4))T
```

### exp
`(exp n)` is equal to e^n.

### expt
`(expt x n)` is equal to x^n. To find roots, call `expt` with a ratio as the second argument.
```lisp
> (expt 27 1/3)3.0
```
But to find square roots, using `sqrt` should be faster.

### fboundp
Returns `t` if there is a function with a given symbol as its name.
```lisp
> (fboundp '+)T
```

### find
Like `member` but `find` is used for sequences.
It takes all keyword arguments, `:key`, `:from-end`, `:start`, `:end`, and `:test`.
```lisp
> (find #\a "cat")#\a
```
Unlike `member`, it returns only the object it was looking for.

### find-if
Like `member-if` but `find-if` is used for sequences. It takes all keyword arguments, `:key`, `:from-end`, `:start`, `:end`, except `:test`.
```lisp
> (find-if #'characterp "ham")#\h
```
Unlike `member-if`, it returns only the object it was looking for.

### float
Converts any real number to a floating-point number.
```
> (mapcar #'float '(1 2/3 .5))(1.0 0.6666667 0.5)
```

### floor
Returns the greatest integer less than or equal to its argument. It also return as a second value the difference between the argument and the first value.

### format
```lisp
(format where-to-print string-template)
```
If `where-to-print` is `t`, then it prints to toplevel.
`string-template`, `~A` is a position to filled, and the `~%` indicates a newline.
```lisp
> (format t "~A ~%" (+ 1 2))3
nil
```
If `where-to-print` is `nil`, then it returns a string what it would have printed. The `where-to-print` also can be a stream.
The `format` function takes a stream, or `t` or `nil`, a format string, and zero or more additional arguments. The fromat string may contain *format directives*, which are preceded by a ~ (tilde). Those format directives will be replaced by the arguments given after the format string.
An `~A` is a placeholder for a value, which will be printed as if by `princ`. An `~S` is just like `~A` but prints objects as if by `prin1`. This mean the `~A` will print its argument in a human-readable form. This will render keywords without the leading : and strings without quatation marks.
```lisp
> (format t "~A" "A")A
NIL
> (format t "~A" :title)TITLE
NIL
```
The number with `~a`, i.e. as the form `~2a` will add paddnig to the right with the amount of that number.
Using `@` after the number, e.x. `~2@a` will justify on the left.
The `~t`, with the form `~nt` where n is an integer, tells `format` to emit enough spaces to move to the tenth column before processing the next `format` parameter. A `~t` doesn't consume any directives.
The `~{` and `~}` tell `format` to consumed the argument, which must be a list, and `format` loops over the list, processing the directives between the `~{` and `~}`, consuming as many elements of the list as needed each time through the list.
The `~%` doesn't consume any arguments but tells `format` to emit a newline. The `~&` also print a newline but it doesn't print a newline if the output stream is already at one.
Format directives can take arguments. `~F`, which is used for printing right-justified floating-point numbers, can take up to five.
Note that `format` doesn't guarantee to round up or round down. So if you want to see rounded number in one particular way, you should round the number explicitly before printing it.
`R` print the number in English.
```lisp
> (format t "~R" 20)"twenty"
```
`$` print monetary.
```lisp
> (format t "~$" 21982)21982
```
`D`, `B`, `O`, and `x`, will print decimal, binary, octal, hexadecimal, respectively.
`~V`for justify a string, but the length is a variable itself, so we use `~v` directive in place of the comma-separated prefix parameters. 
```lisp
> (let ((padding 30))(format nil "~va" padding "foo"))"foo"
```

### funcall 
Apply the function to the given argements. The differnce between this and `apply` is that the arguments need not to be a list at all. 
```lisp
> (funcall #'+ 1 2 3) 
6

> (apply #'+ '(1 2 3))6
``` 

### function
Return the associated object with the name we give to the `funciton`.

```lisp
> (function +)
#<FUNCTION +>

``` 
We can also use `#'` for function.
```lisp
> #'+ 
#<FUNCTION +>

``` 
Since `gethash` returns nil by defaul, a new-made hash table is also, conveniently, an empty set.

### gensym
Create an uninterned symbol. It's useful when writting macros and we want to avoid duplicate variables.

```lisp
(defmacro ntimes (n &rest body)(let ((g (gensym))(h (gensym))) 
`(let ((,h ,n))
(do ((,g 0 (+ ,g 1)))((>= ,g ,h)),@body))))) 
``` 

### get
Since every symbol has a *property-list*, or *plist*, this function takes a symbol and a key of any type, and returns the value associated with the key in the symbol's property list.
```lisp
> (get 'alizarin 'color) 
NIL 
``` 
It uses `eql` to compare keys. If the specified property isn't found, `get` returns `nil`.

To associate a value with a key, you can use `setf` with `get`. 
```lisp
> (setf (get 'alizarin 'color) 'red)RED 
> (get 'alizarin 'color) 
RED 
``` 
Now the `color` property of `alizarin` is `red`.


### getf 
It takes a plist and a symbol and returns the value in the plist following the symbol.

```lisp
> (getf (list :a 1 :b 2 :c 3) :a)
1
``` 

### gethash 
Retrieve the value associated with a given key, we call `gethash` with a key and a hash table. By default, `gethash` returns nil when there is no value associated with the key.
```lisp
> (setf ht (make-hash-table))> (gethash 'color ht) 
NIL 
NIL 
``` 
The first value is the valus associated with the key, and the second is whether the hash table has any value stored under that key. Because the second value is nil, we know that the first nil was returned by default, not because nil was explicitly associated with color.
We can use `setf` to associate a value with a key.

```lisp
> (setf (gethash 'color ht) 'red)
RED 
> (gethash 'color ht) 
RED 
T
``` 
To add a member to a set represented as a hash table, `setf` the `gethash` of it to `t`:

```lisp
> (setf fruit (make-hash-table))
> (setf (gethash 'apricot fruit) t) 
T
``` 
Thento test for membership you just call `gethash`:

```lisp
> (gethash 'apricot fruit)T
T
``` 
It has an optional third argument which will be used as a default value a key doesn't exist. 

### graphic-char-p

Graphic characters are all the characters we can see, plus the space character. 

### hash-table-count

Count the entries in a hash table.

### if 
If the test expression is true, its then expression is evaluated and its value is returned. If the test expression is false, its else expression is evaluated and its value is returned.
If the else expression is omitted, it defaults to nil. 
It'sof the form:

```lisp
(if (test)(then) 
(else))
``` 

### ignore-errors

Thismacro behaves like `progn` if none of its arguments cause an error. But if an error is signalled during the evaluation of one of its arguments, execution will not be interrupted. Instead, the `ignore-errors` expression will immediately return two values: `nil` and the condition that was signalled.
```lisp
> (defun user-input (prompt)(format t prompt)
(let ((str (read-line)))(or (ignore-errors (read-from-string str))
nil))) 
USER-INPUT
> (user-input "Please type an expression> ") 
Please type an expression> #%@#+!!
NIL 
``` 
Herewe want the user to be able to enter an expression but we don't want an error to interrupt execution if the input is syntacticially ill-formed. This function just returns `nil` if the input contains syntax errors.

### import
Import the name to the current package. Beware that the symbol will be shared between 2 packages and can't be distincted.
If the symbol is already in the package, `import` will cause an error.
### incf 
Increases its argument. An expression of the form `(incf x n)` is similar in effect to `(setf x (+ x n))`. The second argument is optional and defaults to 1.

### intern
Makestring argument be a symbol and interns it to a package.
```lisp
> (intern "RANDOM-SYMBOL")RANDOM-SYMBOL

NIL 
``` 
The package argument defaults to the current package, so the preceding expression returns the symbol in the current package whose name is the string "RANDOM-SYMBOL", creating such a symbol if it doesn't already exitst. The second return value shows whether the symbol already existed; in this case, it didn't.

### intersection

Intersection of two sets.
```lisp
> (intersection '(a b c) '(b b c))(B C)
``` 

### labels
The first argument is a list of definitions of new local functions. Each element of the list is of the form `(name parameters . body)`Within hte remainder of the `labels` expression, calling `name` is equivalent to calling `(lambda parameter . body)`.
```lisp
> (labels ((add10 (x) (+ x 10))(consa (x) (cons 'a x))) 
(cons (add10 3)))(A .13) 
``` 
What's different from `let` is that the local functions defined by a `labels` expression can refer to any other functions defined there, including themselves. So it's possible to define recursive local functions this way.
```lisp
> (labels ((len (lst) 
(if (null lst)0

(+ (len (cdr lst)) 1))))(len '(a b c)))3
``` 

### lambda
The function that doesn't have name.
```lisp
(lambda (list-of-parameters)body)
``` 
The lambda expression can be considered as the name of the body of the function.
```lisp
> ((lambda (x) (+ x 100)) 1)101 
``` 

### last 
Returns the last cons in a list. This isn't the same as getting the last element. To get the last element of a list, take the car of `last`.
```lisp
> (last '(a b c))
(C) 
``` 

### let
let allows us to introduce new **local** variables:

```lisp
> (let ((x 1) (y 2)
(+x y))3
``` 
An operator like `let` creates a new *lexical context*. Within this context, outer variables contexts may have thereby become invisible. 
The default value of `let`, and `let*`, is `nil`. Such variables need not be enclosed within lists.
```lisp
> (let (x y)(list x y))
(NILNIL)
``` 

### let* 
The new variables can be depended on the value of another variable.

```lisp
> (let* ((x 1)
(y (+ x 1))) 
(+ x y))
``` 

### lenght
Returns the number of elements in a sequence.
```lisp
> (length '(a b c))
3
``` 

### list 
The same as `cons`:

```lisp
> (list 'a 'b)
(A B)
``` 

### log
It'slike log_n (x), `(log x n)`. If the second argument isn't given, it is like ln(x).
### macroexpand-1

Takes a macro call and generates its expansion.
### make-array

Making array. The first argument is a list of dimensions. 
```lisp
> (setf arr (make-array '(2 3) :initial-element nil))#<Simple-Array T (2 3) BFC4FE> 
``` 
The `:initial-element` is optional. If it is provided, the whole array will be initialized to that value.

The `:element-type` is optional and used to specify the kind of values that the array will contain. Such an array is called a *specialized array*. Since the array isn't a simple array, so we can't use `svref` to refer to its elements.
The `:adjustable` is optional and take boolean value, if it is not-`nil`, the array will adjustable.

### make-hash-table

Creates a hashtabele. `:size` is an optional argument, it will specify the number of elements when we create the hash table (of course the hash table can grow bigger than this size). If we give `:size` an integer, it will have that specify absolute growth or if we give a float, it will specify relative growth.
`:test` specify that notion of equality for keys. By default they use `eql`, but we can make it uses `eq`, `equal`, or `equalp` instead. 
```lisp
> (setf writers (make-hash-table :test #'equal)) 
> (setf (gethash '(Great Writer) writers) t) 
``` 

### make-instance

Makeinstances of the first argument, i.e. the class name.
```lisp
(setf c (make-instance 'circle))
``` 

### make-pathname

A pathname has `host`, `device`, `directory`, `name`, `type`, and `version`. We can just specifythe name and let the rest of the parameter default.
```lisp
> (setf path (make-pathname :name "myfile")) 
#P"myfile"
``` 

### map
It takes the output-type as first argument (`'list`, `'vector`, or `'string`):
```lisp
> (defparameter foo '(1 2 3))> (map 'list (lambda (it) (* 10 it)) foo) 
``` 

### mapc 
Like`mapcar` but it doesn't cons up a new list as a return value, so the only reason to use it is for side-effects. It is more flexible than `dolist`, because it can traverse multiple lists in parallel.
```lisp
> (mapc #'(lambda (x y) (format t "~A ~A " x y)) '(hip flip slip) '(hop flop slop))HIP HOP FLIP FLOP SLIP SLOP 
(HIPFLIP SLIP) 
``` 
It always returns its second argument. 

### mapcar
Takea function and one or more lists, and returns the result of applying the function to elements taken from each list, until some list runs out.
```lisp
> (mapcar #'(lambda (x) (+ x 10)) '(1 2 3))(11 12 13)> (mapcar #'list '(a b c) '(1 2 3 4))((A 1) (B 2) (C 3))
``` 

### maphash 
Takes a function of two arguments and a hash table. The function will be called on every key/value pair in the table, in no particular order.
```lisp
> (setf (gethash 'shape ht) 'spherical 
(gethash 'size ht) 'giant)GIANT
> (maphash #'(lambda (k v) (format t "~A = ~A~%" k v)) ht)SHAPE = SPHERICAL

SIZE= GIANT
COLOR = RED ; from those `gethash` example.
NIL 
``` 
It always returns `nil`, but you can save the values by passing a function that will accumulate them in a list. 

### maplist 
Takethe same arguments as `mapcar`, but calls the function on successive cdrs of the lists. 
```lisp
> (maplist #'(lambda (x) x) '(a b c))((A B C) (B C) (C))
``` 

### max
Returns the maximum of their arguments.
```lisp
> (max 1 2 3 4 5)
5
``` 

### member
Returns the part of the list beginning with the object it was looking for.
```lisp
> (member 'b '(a b c))(B C)
``` 
By default, it uses `eql` to compare objects. We can change that using `:test` keyword argument.
```lisp
> (member '(a) '((a) (z)) :test #'equal)((A)(Z))
``` 
We can also use `:key` argument to specify a function to be applied to each element before comparison.
```lisp
> (member'a '((a b) (c d)) :key #'car) 
((A B) (C D))
``` 
In the above, we asked if there was an elements whose car was a.

### member-if

Finds an element satisfying an arbitary predicate -- like `oddp`.

```lisp
> (member-if #'oddp '(2 3 4))(3 4)
``` 

### min
Returns the minimum of its arguments.
```lisp
> (min 1 2 3 4 5)
1
``` 

### mismatch
Returns position in the first sequence where the first sequence and second sequence begin to mismatch. Returns NIL if they match entirely. Other parameters are `:from-end`, `:start1`, `:start2`, `:end1`, and `end2`.

### mod
Returns the remainder after division. It is the second value of what `float` would return.

```lisp
> (mod 23 5)3
> (mod 25 5)0
``` 

### most-positive-fixnum 
It'sa constant of the largest positive number an implementation can represent without having to use bignums.
### most -negative-fixnum
Like`most-positive-fixnum` but for the leastest negative number.
### multiple-value-bind
Receive multiple values. 
```lisp
> (multiple-value-bind (x y z) (values 1 2 3)(list x y z))
(1 23)> (multiple-value-bind (x y z) (values 1 2)(list x y z))
(1 2NIL)
``` 
If there are more variables than values, the leftover ones will be `nil`. If there are more values than variables, the extra values will be discarded.

### multiple-value-call
Passon multiple values as the arguments to a second function.
```lisp
> (multiple-value-call #'+ (values 1 2 3))6
``` 

### multiple-value-list
Turns multiple values to a list.

```lisp
> (multiple-value-list (values 1 2 3)) 
(1 23)
``` 
The reverse is **values-list** which turns a list to multiple values.
### multiple-value-setq
Like `setf` but can set multiple variables.

### next-method-p

See `defmethod`. Checking whether there is the primary method or not.
### null 
Returns true if its argument is an empty list or `nil`.
```lisp
> (null nil)T
``` 

### not
Returns true if its argument is false. 
```lisp
> (not nil) 
T
``` 

### nth
Findthe element at a given position in a list. It's equal to car of nthcdr. 
```lisp
> (nth 0 '(a b c))
A
``` 

### nthcdr
Findthe nth cdr.

```lisp
> (nthcdr 2 '(a b c)) 
(C) 
``` 

### nth-value

Takethe nth value from the caller that returns multiple values.
```lisp
> (nth-value 0 (values :a :b :c))
:A
> (nth-value 2 (values :a :b :c))
:C
> (nth-value 9 (values :a :b :c))
NIL 
``` 

### open 
Opening a file. It takes a pathname (or a string) and a large number of optional keyword arguments, and if successful, returns a stream that points to the file.
The `:direction` argument specify what we want to do with the stream, by using `:input` for writing to the stream, `:output` for reading from the stream, or `:io` for both. If we use `:output`, the `:if-exists` arguments says what to do if the destination file already exists; usually it should be `:supersede`.
The `:element-type` is a type of element of the stream. Using a subtype of `integer` -- most often `unsigned-byte`, will create a binary stream. By specifying just `unsigned-byte`, you let the operating system choose the length of a byte. If you specifically wanted to read or write 7-bit integers, for example, you would use `(unsigned-byte 7)` as the `:element-type` instead.
So to create a stream on which you can write to the file "myfile", we can do:
```lisp
> (setf path (make-pathname :name "myfile")) 
> (setf str (open path :direction :output :if-exists :supersede))
#<Stream C017E6>

``` 
Now we can write to it by use it as `format`'s first argument.
Notethat we have to use `close` to guarantee that the content is created.

### or 
It takes any number of arguments and stops as soon as it finds an argument that is true.

```lisp
> (or t (+ 1 2))
T
``` 

### pairlis 
Constructs an associated list of keys and a list of value.
```lisp
> (pairlis '(:foo :bar) '("foo" "bar"))((:BAR . "bar") (:FOO . "foo"))
``` 

### pop
It removes and returns the first elment of its argument (list). 
```lisp
> (setf x '(a b))
(A B)> (pop x)(A) 
``` 
It, `(pop lst)`, can be defined as
```lisp
(let((x (car lst)))(setf lst (cdr lst)) 
x) 
``` 

### position
Returns the position of an element in a sequence, `nil` if it is not found.
```lisp
> (position #\a "fantasia") 
1
> (position #\a "fantasia" :start 3 :end 5)4
> (position #\a "fantasia" :from-end t)7
``` 
The `:key` arguments, apply to many sequence functions as well, is a function that is applied to each element of a sequence before it is considered.
```lisp
> (position 'a '((c d) (a b)) :key #'car) 
1
``` 
The `:test` argument is a function of two arguments, and defines what it takes for a successful match. It always defaults to `eql`. If we're trying to match a list, we might want to use `equal` instead.
```lisp
> (position '(a b) '((a b) (c d)))NIL 
> (position '(a b) '((a b) (c d)) :test #'equal) 
0
``` 
The `:test` argument can be any function of two arguments.
```lisp
> (position 3 '(1 0 7 5) :test #'<) 
2
``` 

### position-if

Takes a function and a sequence, and returns the position of the first element satisfying the function.

```lisp
> (position-if #'oddp '(2 3 4 5))
1
``` 
It takes all the keyword arguments, `:key`, `:from-end`, `:start`, `:end`, except `:test`.
### parse-integer

Takes a string representing an integer and return that integer. It will signal an error if it can't parse an integer out of the string or if there's any non-numeric junk in the string. However, the optional keyword argument `:junk-allowed`, if it's set to `t`, will make `parse-integer` return `nil` if it can't find an integer in the string.
```lisp
> (parse-integer " 42 is forty-two" :junk-allowed t)
42
3
``` 

### prin1
See `princ`. It generates output from programs.

```lisp
> (prin1 "Hello")
"Hello"
"Hello"
``` 

### princ
See `prin1`. It generates output from people.
```lisp
> (princ "Hello")
Hello
"Hello"
``` 

### print
It prints Lisp objects in a form that can be read back in by the Lisp reader. The second argument, like `format`, can be a stream that `print` will print the data to.

### progn
Takes any number of expressions, evaluates them in order, and returns the value of the last. 
```lisp
> (progn 
(+ 1 1) 
(+ 2 2))4
``` 
Since only the value of the last expression is returned, the use of `progn` (or any block) implies side-effects.

### push 
It pushes the first argument onto the front of the second argument (list). `push` prepends *items* to the list that is stored in *place*, stores the resulting list in *place*, and returns the list.
```lisp
> (setf x '(b)) 
(B) 
> (push 'a x)
(A B)
``` 
It, `(push obj lst)`, is equivalent to `(setf lst (cons obj lst))` except that the subforms of *lst* are evaluated only once, and *obj* is evaluated before *lst*.

### Pushnew 
The same as `push` but uses `adjoin` instead of `cons`, i.e., it doesn't add the same object to the list.

```lisp
> (let ((x '(a b)))
(pushnew 'c x)
(pushnew 'a x)
x) 
(C AB)
``` 

### read 
Thisreads exactly one expression, and stop at the end of it. What it reads has to be valid Lisp syntax. You may want to avoid using `read` directly to process user input. Instead, use `read-line` to get what the user typed, then called `read-from-string` on the resulting string.

### read-byte

For reading a binary stream.

### read-delimited-list
Its first argument is the character to treat as the end of the list. See `read`.

### read-from-string
Takes a string and returns the first expression read from it.
```lisp
> (read-from-string "a b c")A
2
``` 
It also returns a second value, a number indicating the position in the string at which it stopped reading.

In the general case, it can take two optional and three keyword arguments. The two optional arguments are the third and the fourth arguments to read; whethere an end-of-file (or in this case, string) should cause an error, and if not, what to return instead. The keyword parameters `:start` and `:end` can be used to delimit the portion of the string read.

### read-line

Readthe file from the stream. It reads all the characters up toa newline, returning them in a string. It takes an optional stream argument; if the stream is omitted, it will default to `*standard-input*`. This is the function to use if you want verbatim input.
In the general case, `read-line` takes four optional arguments: a stream; an argument to tell whether or not to cause an error on encounting end-of-file; what to return instead if the previous argument is `nil`; and a fourth argument that can usually be ignored.
```lisp
> (defun pseudo-cat (file)(with-open-file (str fire :direction :input) 
(do ((line (read-line str nil 'eof) 
(read-line str nil 'eof)))((eql line 'eof))(format t "~A~%" line))))
``` 
The above function read the contents of a file at the toplevel. 

### rem
Returns the second value that `truncate` would return. 

### remf 
Remove an element from a plist.

### remhash 
Remove an entry from a hash table.
```lisp
> (fruit (make-hash-table)) 
> (setf (gethash 'apple fruit) t)
> (remhash 'apple fruit) 
T
``` 
The return value shows whether there was an entry to remove; in this case there was, if it wasn't, it would return `nil`.

### return
Returns its argument as the value of an enclosing block named `nil`:

```lisp
> (block nil
(return 27))
27
``` 
ManyCommon Lisp operators that take a body of expressions implicitly enclose the body in a block named nil. All iteration constructs do, for example.
```lisp
> (dolist (x '(a b c d e))(format t "~A " x)(if (eql x 'c)
(return 'done))) 
A B C
DONE
``` 

### rotatef
Rotate values between places. For example, `(rotatef a b)` will swap place of a and b and return nil.

### round
Returns the nearest integer to its argument. When the argument is equidistant from two integers, it rounds to the nearest even digit. 
```lisp
> (mapcar #'round '(-2.5 -1.5 1.5 2.5))(-2 -2 2 2) 
``` 
It also return as a second value the difference between the argument and the first value.
### row-major-aref

See `aref`. `(row-major-aref)` is equivalent to `(aref i i i ...)`.


### search
Search the second sequence for a subsequence matching the first sequence. Returns position in the second sequence, or NIL. Has the `:from-end`, `:start1`, `:start2`, `:end1`, `:end2`, `:test`, and other parameters.
```lisp
> (search "we" "If we can't be free we can at least be cheap")3
``` 

### setf 
If the first argument is a symbol that is not the name of a local variable, it is taken to be a global variable. Else it is taken as local variable. That is, it implicitylf creates global variables.
```lisp
> (setf *glob* 98)
98
> (let ((n 10)) 
(setf n 2)
n) 
2
``` 
It can also referred in place of the first argument if it is expreesion.
```lisp
> (setf x (list 'a 'b 'c))(A BC)> (setf (car x) 'n)
N
> x 
(N BC)
``` 
`setf` also can be used with `char` or `aref` to replace elements.

```lisp
> (let ((str (copy-seq "Merlin")))(setf (char str 3) #\k) 
str) 
"Merkin" 
``` 

### set-exclusive-or
Remove elements that are in both lists.

### set-difference

The difference of two sets. 
```lisp
> (set-difference '(a b c d e) '(b e)) 
(A DR)
``` 

### set-macro-character
It takes a character and a function, and thereafter when `read` encounters the character, it returns the result of calling that function.
We could define `'`, the quote, as: 
```lisp
(set-macro-character #\' 
#'(lambda (stream char)(list (quote quote) (read stream t nil t))))
``` 
When`read` encounters an instance of `'` in a normal context, it will return the result of calling this function on the current stream and character. (The function ignores this second parameter, which will always be the quote character.) So when `read` sees `'a`, it will return `(quote a)`.

### set-dispatch-macro-character

Like`set-macro-character` except that it takes two character arguments.
```lisp
(set-dispatch-macro-character #\# #\?
#'(lambda (stream char1 char2)
(list 'quote

(let ((lst nil))(dotimes (i (+ (read stream t nil t) 1)) 
(push i lst))(nreverse lst)))))
``` 
Thisreads `#?n` as a list of all the integers from 0 to n.

### shiftf
Shift the values to the left and the original value of the first argument is returned.

### signum
Returns either 1, 0, or -1, depending on whether its argument is positive, zero, or negative.

### slot-value

Access the valu of the given instance and the given slot. We can also use `setf` to set the slots in this instance.
```lisp
> (setf c (make-instance 'circle))> (setf (slot-value c 'radius) 1)
1
``` 

### some 
The same as `every`. Takes a predicate and one or more sequences. When given just one sequence, it test whether the elements satisfy the predicate. If they are given more than one sequence, the predicate must take as many arguments as there are sequennces, and arguments are drawn one at a time from all the sequences. If the sequences are of different lengths, the shortest one determines the number of test performed.
```lisp
> (some #'evenp '(1 2 3))T
``` 

### sort 
It takes a sequence and a comparison function of two arguments, and returns a sequence with the same elements, sorted according to the function.
```lisp
> (sort '(0 2 1 3 8) #'>)(8 32 1 0) 
``` 
Beware that `sort` modify the given sequence so you may prefer to copy the sequence before sorting.

### special 
Declare a variable as dynamic scope. We have to declare it to be special in any context where it occurs.

```lisp
> (let ((x 10)) 
(defun foo ()
(declare (special x)) 
x))
``` 
Thisx in `foo` is now dynamic scope.
```lisp
> (let ((x 20)) 
(declare (special x))(foo))20
``` 

### sqrt 
Returns the square root of its argument.

### subst
Replaces elements in a tree.
```lisp
> (subst 'y 'x '(and (intergerp x) (zerop (mod x 2)))) 
(AND(INTEGERP Y) (ZEROP (MOD Y 2)))
``` 

### subseq
Copys part of a sequence. The second argument is the position of the first element to be included, and the third argument (optional) is the position of the first element not to be included.
```lisp
> (subseq '(a b c d) 1 2)(B) 
> (subseq '(a b c d) 1)(B CD)
``` 
Thisfunction also `setf`-able. Note that when working with string and the new sequence are not of equal length, the shorter length determines the number of elements that are replaced. That is, the string isn't "stretchable".
```lisp
* (defparameter *my-string* (string "Karl Marx"))
*MY-STRING* 
* (subseq *my-string* 0 4)"Karl" 
* (setf (subseq *my-string* 0 4) "Harpo") 
"Harpo"
* *my-string*

"Harp Marx" 
* (subseq *my-string* 4) 
" Marx"
* (setf (subseq *my-string* 4) "o Marx")"o Marx" 
* *my-string*

"Harpo Mar" 
``` 

### substitute

It also have `substitute-if`, `substitude-if-not`, and `nsubstitute` and its derivatives.

Retrun a sequence of the same kind as a argugment given to `substitute` with the same elements, except that all elements equal to the second argument are replaced with the first argument.
```lisp
> (substitute #\o #\x "hellx") 
"hello"
``` 
### svref
Retrieves an element of vector. We can use `aref` like an array but using `svref` is faster. 
```lisp
> (defvar vec (vector 1 'b "C"))
VEC 
> (svref vec 2) 
"C" 
``` 

### string-equal

We can use `equal` to compare two string, but `string-equal` ignores case.
```lisp
> (equal "fred" "Fred")NIL 
> (string-equal "fred" "Fred") 
T
``` 

### string-trim

Returns a substring of its second argument where all characters that are in the first argument are removed off the beginnig and the end. The first argument can be any sequence of characters. There are also `string-left-trim` and `string-right-trim` which remove from the beginning and end, respectively.
```lisp
> (string-trim " " " trime me ")
"trime me"
> (string-trim " et" " trim me ")
"rimm"
> (string-right-trim '(#\Space #\e #\t) " trim me ")
" trim m"
``` 
Notethat when there is no characters to remove, the result may be either string or a copy of it, at the implementation's discretion. 

### symbol-function

Returns the function of the given symbol. 
```lisp
> (symbol-function '+)#<Compiled-Function + 17BA4E>
``` 
We can define a new global function by setting the `symbol-function` of some name to a function.
```lisp
> (setf (symbol-function 'add2)#'(lambda (x) (+ x 2))) 
``` 
The above code is like we use `defun`. 

### symbol-name

Returns the name of the symbol as a string.
```lisp
> (symbol-name 'abc)"ABC"
``` 
You can do the same with `string` function.
```lisp
> (string 'abc) 
"ABC"
``` 

### symbol-plist

Returns the property list of a symbol. The property lists aren't represented as assoc-lists, though they are used the same way.
### random
Thisfunction takes an interger or floating-point number and, `(random n)` returnns a number greater than or equal to zero and less than n, and of the same type as n.

### read 
Readthe toplevel.

```lisp
(defun ask (string)
(format t "~A" string) 
(read))(ask"Enter the number") 
``` 
Thiscode will ask for the input.

`read` also takes 4 optional arguments respectively: the stream; whether `read` should signal an error if the next thing it sees is the end-of-file, t or nil; whether what to return instead of generating an error (it can be `nil`); and whether the call to `read` is a recursive or not. See `set-macro-character` for an example.

### reduce
It takes at least two arguments, a function and a sequence. The function must be a function of two arguments. In a simplest case, ti will be called initially with the first two elements, and thereafter with successive elements as the second argument, and the value it returned last time as the first. The value returned by the last call is returned as the value of the `reduce`.
Which means that `(reduce #'fn ' (a b c d))` is equivalent to `(fn (fn (fn 'a 'b) 'c) 'd)`.
We can use `reduce` to extend functions that only take two arguments.

```lisp
> (reduce #'intersection '((b r a d 's) (b a d) (c a t))) 
(A) 
``` 

### remove
It takes an object and a list and returns a new list containing everything but that object. The original list is unchanged.

```lisp
> (setf lst '(c a r a t))(C AR A T) 
> (remove 'a lst)
(C RT)> lst
(C AR A T) 
``` 
For a alist, we can remove only one elment with `:count`: 
```lisp
> (defvar *my-alist* '((:bar "bar2") (:team . "team") (:bar "bar")))
> (remove :bar *my-alist* :key 'car :count 1)((:TEAM . "team") (:BAR . "bar"))
;; if we use (remove :bar *my-alist* :key 'car) the result would be ((:TEAM . "team")).

``` 

### remove-duplicates 
Removes all the duplicated elements except the last of each occurrence of any element of a sequence.
```lisp
> (remove-duplicates "abracadabra") 
"cdbra"
``` 
It also takes all the keyword arguments, `:key`, `:from-end`, `:start`, `:end`, and `:test`. 

### remove-if

Removes the element that match the test.

### reverse 
Returns a sequence with the same elements as its argument, but in the reverse order:

```lisp
> (reverse '(a b c))(C BA)
``` 

### tagbody 
Within it you can use gotos, `go`. Atoms appearing in the body are interpreted as labels; giving such a label to go sends control to the expression following it.
```lisp
> (tagbody
(setf x 0)
top
(setf x (+ x 1))(format t "~A " x)(if (< x 10) (go top))) 
1 2 3 4 5 6 7 8 9 10
NIL 
``` 

### terpri
Prints a newline.
### the
Declare that the value of an expression will be of a certain type.

```lisp
(defun poly (a b x)
(declare (fixnum a b x))(the fixnum (+ (the fixnum (* a (the fixnum (expt x 2))))(the fixnum (* b x)))))
``` 
The better option of using `the`s is using macro to insert such declarations.

### throw
Throws a tag. See also `catch`. When called, a throw with a given tag will pass control through (and thereby kill) any catches with other tags in order to reach the one with the matching tag. If there is no pending `catch` with the right tag, the `throw` causes an error.

### truncate
Returns the integer component of any real number.

```lisp
> (truncate 1.3)
1
0.29999995
``` 
The second return value is the original argument minus the first return value.

### typecase
See `case`. The keys in each clause should be type specifiers, and the value of the first argument is compared to the keys using `typep` instead of `eql`.

### typep
In common lisp, values have types, not variables. An object always has more than one type. The type `t` is the supertype of all types, so everything is of type `t` (even `nil` is of type `t`).
`typep` takes an object and a type specifier, and returns true if the object is of that type.
```lisp
> (typep 27 'integer) 
T
``` 

### union
Union of two set.

```lisp
> (union '(a b c) '(c b s)) 
(A CB S)
``` 
Notethat set doesn't have order.
### unless
It takes an expression and a body of code. The body will be evaluated if the test expression returns false. See `when`.

### unwind-protect

Takes any number of arguments and returns the value of the first. However, the remaining expressions will be evaluated even if the evaluation of the first is interrupted.
```lisp
> (setf x 1)1
> (catch 'abort

(unwind-protect

(throw 'abort 99)(setf x 2)))
99
> x 
2
``` 
Whenever certain actions have to be follewed by some kind of cleanup or reset, `unwind-protect` may be useful.

### use-package

Import all the symbol from the specific package to the current package. All the symbols exported by the specific package can be used without any qualifier and if the symbol is already in the current package, it will cause an error).

### values
Returns multiple values. It returns exactly the values you give it as arguments.
```lisp
> (values 'a nil (+ 2 4))A
NIL 
6
``` 
If a`values` expression is the last thing to be evaluated in the body of a function, its return values become those of the function. Multiple values are passed on intact through any number of returns.
```lisp
> ((lambda () ((lambda () (values 1 2)))))1
2
``` 
However, if something is expecting only one value, all but the first will be discarded.

```lisp
> (let ((x (values 1 2)))x) 
1
``` 
By using `values` with no arguments, it's possible to return no values (not the same as `nil`). In that case, something expecting one will get `nil`.
```lisp
> (values)> (let ((x (values))) 
x) 
NIL 
``` 

### values-list

Turns a list to multiple values.

```lisp
> (values-list '(1 2 3)) 
1
2
3
``` 
The reverse is **multiple-value-list** which turns multiple values to a list.

### vector
Returns a simple vector of arguments we give it.

```lisp
> (vector "a" 'b 3)
#("a" B 3)
``` 
We can also use the reader macro `#()`.
```lisp
>#(12 3)#(1 2 3) 
``` 

### vector-pop

Return the element of vector its fill pointer points to.
```lisp
;; see the vector-push example below
> (vector-pop *v*)
3
> (vector-pop *v*)
4
``` 

### vector-push

It'sof the form `(vector-push foo vector)`. Replace the vector element pointed to by the fill pointer by foo. Can be destructive. 
```lisp
> (defparametre *v* (make-array 2 :fill-pointer 0))
*V* 
> (vector-push 4 *v*) 
0 ; index
> (vector-push 3 *v*) 
1 ; index
> (vector-push 2 *v*) 
NIL ; the vector is full 
> *v*
#(4 3) 
``` 

### with-hash-table-iterator
A macro that turns its first argument into an iterator that or each invocation returns three values per hash table entry - a generalized boolean that's tru if an entry is returned, the key of the entry, and the value of the entry. If there are no more entries, only one value is returned - `nil`.
As an example, we can define `maphash` as the following:
```lisp
(defun maphash (function hash-table)(with-hash-table-iterator (next-entry hash-table)
(loop (multiple-value-bind (more key value) (next-entry) 
(unless more (return nil))
(funcall function key value))))) 
``` 
Notethat it is unspecified what happens if any of the implicit interior state of an iteration is returned outside the dynamic extent of the `with-hash-table-iterator` form such as by returning some closure over the invocation form.

### with-open-file

The first argument should be a list containing a variable name followed by arguments we want to give to `open`. After this, it takes a body of code, which is evaluated with the variable bound to a stream created by passing the remaining arguments to `open`. Afterward, the stream is automatically closed. The `with-open-file` macro puts the `close` within an `unwind-pretect`, so the file is guaranteed to get closed, even if an error interrupts the evaluation of the body.
```lisp
> (with-open-file (str path :direction :output :if-exists :supersede)
(format str "Something~%")) 
``` 

### with-output-to-string
It creates a string out of the printed representation of various object, the value of this macro is a string containing everything that was output to the string stream within the body to the macro.
```lisp
> (with-output-to-string (stream)
(dolist (char '(#\Z #\a #\ p #\p #\a #\, #\Space)) 
(princ char stream))(format stream "~s - ~s" 1940 1993))"Zeppa, 1940 - 1993"
``` 

### with-standard-io-syntax 
You can use this when printing to or reading from a file. It ensures that certain variables that affect the behavior of the stream are set to their standard values.

### when 
It takes an expression and a body of code. The body will be evaluated if the test expression returns true.

```lisp
> (when (oddp that)
(format t "Hmm, that's odd.")
(+ that 1)) 
Hmm,that's odd.

``` 
The above code is equivalent to
```lisp
> (if (oddp that)
(progn 
(format t "Hmm, that's odd.")(+ that 1)))
``` 

### write-byte

For writing a binary stream.

### write-to-string

Converts number to string. It accepts `:base` argument which specify the base of the number. 
```lisp
> (write-to-string 250)"250"
> (write-to-string 250 :base 5)"2000" 
``` 

### y-or-n-p
It takes a string and read the input and turn that input to `t` or `nil` accordingly. It will reprompt if you enter something that doesn't start with y, Y, n or N.

### zerop
Return true if its argument is zero (take only number argument).
```lisp
> (zerop 0)T
```

### Library

## UIOP
It has many functions to work on strings.
```lisp
strcat, string-prefix-p, string-enclosed-p, first-char, last-char, split-string, stripln
```

## str
More functions to work with strings.
```lisp
trim, words, unwords, lines, unlines, concat, split, shorten, repeat, replace-all, strats-with?, ends-with?, blankp, emptyp, ...
```

## parse-float
It parse floating point number. It doesn't use `read-from-string` as its implementation so it is safe to use.
